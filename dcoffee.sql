--
-- File generated with SQLiteStudio v3.4.4 on พฤ. ต.ค. 12 10:11:59 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: category
DROP TABLE IF EXISTS category;

CREATE TABLE IF NOT EXISTS category (
    category_id   INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    category_name TEXT (50) NOT NULL
);

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         1,
                         'coffee'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         2,
                         'dessert'
                     );

INSERT INTO category (
                         category_id,
                         category_name
                     )
                     VALUES (
                         3,
                         'candy'
                     );


-- Table: customer
DROP TABLE IF EXISTS customer;

CREATE TABLE IF NOT EXISTS customer (
    customer_id   INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    customer_name TEXT,
    customer_tel  TEXT    UNIQUE
);

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel
                     )
                     VALUES (
                         1,
                         'menoi',
                         '0888868888'
                     );

INSERT INTO customer (
                         customer_id,
                         customer_name,
                         customer_tel
                     )
                     VALUES (
                         5,
                         'new',
                         '0887679156'
                     );


-- Table: product
DROP TABLE IF EXISTS product;

CREATE TABLE IF NOT EXISTS product (
    product_id          INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    product_name        TEXT (50) UNIQUE,
    product_price       DOUBLE    NOT NULL,
    product_size        TEXT (5)  DEFAULT SML
                                  NOT NULL,
    product_sweet_level TEXT (5)  DEFAULT (123) 
                                  NOT NULL,
    product_type        TEXT (5)  DEFAULT HCF
                                  NOT NULL,
    category_id         INTEGER   DEFAULT (1) 
                                  NOT NULL
                                  REFERENCES category (category_id) ON DELETE SET NULL
                                                                    ON UPDATE CASCADE
);

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        1,
                        'Espresso',
                        30.0,
                        'SML',
                        '0123',
                        'HCF',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        2,
                        'Americano',
                        40.0,
                        'SML',
                        '012',
                        'HC',
                        1
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        3,
                        'เค้กชิฟฟ่อนช็อกโกแลต',
                        50.0,
                        '-',
                        '-',
                        '-',
                        2
                    );

INSERT INTO product (
                        product_id,
                        product_name,
                        product_price,
                        product_size,
                        product_sweet_level,
                        product_type,
                        category_id
                    )
                    VALUES (
                        4,
                        'บัตเตอร์เค้ก',
                        60.0,
                        '-',
                        '-',
                        '-',
                        2
                    );


-- Table: reciept
DROP TABLE IF EXISTS reciept;

CREATE TABLE IF NOT EXISTS reciept (
    reciept_id   INTEGER  PRIMARY KEY ASC AUTOINCREMENT,
    created_date DATETIME DEFAULT ( (datetime('now', 'location') ) ) 
                          NOT NULL,
    total        REAL,
    cash         REAL,
    total_qty    INTEGER,
    User_id      INTEGER  REFERENCES user (user_id) ON DELETE RESTRICT
                                                    ON UPDATE CASCADE
                          NOT NULL,
    customer_id  INTEGER  REFERENCES customer (customer_id) ON DELETE RESTRICT
                                                            ON UPDATE CASCADE
);

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        1,
                        '2023-10-01 22:18:01',
                        160.0,
                        1000.0,
                        4,
                        6,
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        11,
                        '2023-10-02 13:42:51',
                        100.0,
                        100.0,
                        1,
                        10,
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        12,
                        '2023-10-05 10:15:19',
                        110.0,
                        1000.0,
                        3,
                        10,
                        1
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        13,
                        '2023-10-12 03:01:20',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        14,
                        '2023-10-12 03:01:20',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        15,
                        '2023-10-12 03:01:21',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        16,
                        '2023-10-12 03:01:21',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        17,
                        '2023-10-12 03:01:23',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        18,
                        '2023-10-12 03:01:26',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        19,
                        '2023-10-12 03:01:27',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        20,
                        '2023-10-12 03:01:29',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        21,
                        '2023-10-12 03:01:30',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        22,
                        '2023-10-12 03:01:30',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        23,
                        '2023-10-12 03:01:30',
                        610.0,
                        0.0,
                        14,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        24,
                        '2023-10-12 03:01:58',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        25,
                        '2023-10-12 03:01:58',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        26,
                        '2023-10-12 03:01:58',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        27,
                        '2023-10-12 03:01:59',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        28,
                        '2023-10-12 03:01:59',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );

INSERT INTO reciept (
                        reciept_id,
                        created_date,
                        total,
                        cash,
                        total_qty,
                        User_id,
                        customer_id
                    )
                    VALUES (
                        29,
                        '2023-10-12 03:02:00',
                        210.0,
                        0.0,
                        7,
                        6,
                        0
                    );


-- Table: reciept_detail
DROP TABLE IF EXISTS reciept_detail;

CREATE TABLE IF NOT EXISTS reciept_detail (
    reciept_detail_id INTEGER PRIMARY KEY ASC AUTOINCREMENT,
    product_id        INTEGER REFERENCES product (product_id) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE,
    product_name      TEXT,
    product_price     REAL,
    qty               INTEGER,
    total_price       REAL,
    reciept_id        INTEGER
);

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               1,
                               1,
                               'Espresso',
                               30.0,
                               2,
                               60.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               2,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               3,
                               2,
                               'Americano',
                               40.0,
                               1,
                               40.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               4,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               5,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               6,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               7,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               1
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               8,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               12
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               9,
                               2,
                               'Americano',
                               40.0,
                               2,
                               80.0,
                               12
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               10,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               11,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               12,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               13,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               14,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               15,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               16,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               17,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               18,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               19,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               20,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               21,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               22,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               23,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               13
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               24,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               25,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               26,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               27,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               28,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               29,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               30,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               31,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               32,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               33,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               34,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               35,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               36,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               37,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               14
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               38,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               39,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               40,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               41,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               42,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               43,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               44,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               45,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               46,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               47,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               48,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               49,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               50,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               51,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               15
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               52,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               53,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               54,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               55,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               56,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               57,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               58,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               59,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               60,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               61,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               62,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               63,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               64,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               65,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               16
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               66,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               67,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               68,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               69,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               70,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               71,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               72,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               73,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               74,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               75,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               76,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               77,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               78,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               79,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               17
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               80,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               81,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               82,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               83,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               84,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               85,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               86,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               87,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               88,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               89,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               90,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               91,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               92,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               93,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               18
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               94,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               95,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               96,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               97,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               98,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               99,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               100,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               101,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               102,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               103,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               104,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               105,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               106,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               107,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               19
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               108,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               109,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               110,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               111,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               112,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               113,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               114,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               115,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               116,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               117,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               118,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               119,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               120,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               121,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               20
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               122,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               123,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               124,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               125,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               126,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               127,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               128,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               129,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               130,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               131,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               132,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               133,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               134,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               135,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               21
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               136,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               137,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               138,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               139,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               140,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               141,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               142,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               143,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               144,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               145,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               146,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               147,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               148,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               149,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               22
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               150,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               151,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               152,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               153,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               154,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               155,
                               4,
                               'บัตเตอร์เค้ก',
                               60.0,
                               1,
                               60.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               156,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               157,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               158,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               159,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               160,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               161,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               162,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               163,
                               3,
                               'เค้กชิฟฟ่อนช็อกโกแลต',
                               50.0,
                               1,
                               50.0,
                               23
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               164,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               165,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               166,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               167,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               168,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               169,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               170,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               24
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               171,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               172,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               173,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               174,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               175,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               176,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               177,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               25
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               178,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               179,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               180,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               181,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               182,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               183,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               184,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               26
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               185,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               186,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               187,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               188,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               189,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               190,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               191,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               27
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               192,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               193,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               194,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               195,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               196,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               197,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               198,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               28
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               199,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               200,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               201,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               202,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               203,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               204,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );

INSERT INTO reciept_detail (
                               reciept_detail_id,
                               product_id,
                               product_name,
                               product_price,
                               qty,
                               total_price,
                               reciept_id
                           )
                           VALUES (
                               205,
                               1,
                               'Espresso',
                               30.0,
                               1,
                               30.0,
                               29
                           );


-- Table: user
DROP TABLE IF EXISTS user;

CREATE TABLE IF NOT EXISTS user (
    user_id       INTEGER   PRIMARY KEY ASC AUTOINCREMENT,
    user_login    TEXT (50) UNIQUE,
    user_gender   TEXT (3)  NOT NULL,
    user_password TEXT (50) NOT NULL,
    user_role     INTEGER   NOT NULL,
    user_name     TEXT (50) 
);

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     6,
                     'werapan',
                     'M',
                     'password',
                     1,
                     'Worawit'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     10,
                     'admin',
                     'M',
                     'password',
                     0,
                     'Administrator'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     11,
                     '3r3r3r',
                     'M',
                     'r3r3r',
                     0,
                     '3r3r3r'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     12,
                     'r32r3',
                     'M',
                     '32r32r',
                     0,
                     '32r3r'
                 );

INSERT INTO user (
                     user_id,
                     user_login,
                     user_gender,
                     user_password,
                     user_role,
                     user_name
                 )
                 VALUES (
                     13,
                     '345r24435',
                     'M',
                     '34t43t34t3',
                     0,
                     '34t43t43t'
                 );


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
